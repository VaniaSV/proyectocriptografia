from Crypto.Util.number import getPrime
import random
def eea(r0,r1):
    if r0 == 0:
        return (r1, 0, 1)
    else:
        g, s, t = eea(r1 % r0, r0)
        return (g, t - (r1 // r0) * s, s)

def modinv(a, m):
    g, x, y = eea(a, m)
    if g != 1:#Revisa que el resto sea 1
        return 0
    else:
        return x % m

p, q = [getPrime(10) for i in range(2)] # con esto generamos p y q para RSA
n=p*q
phin = (p-1)*(q-1)
e= random.randint(1,phin)
d=modinv(e,phin)
while(d==0):
    e= random.randint(1,phin)
    d=modinv(e,phin)   


f = open("llaves.txt", "w")
f.write(str(n)+","+str(e))
f.close()

x = open("llavesecreta.txt", "w")
x.write(str(d))
x.close()
